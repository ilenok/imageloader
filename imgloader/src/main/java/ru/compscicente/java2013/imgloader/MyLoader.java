package ru.compscicenter.java2013.imgloader;

import org.jsoup.Jsoup;
import org.jsoup.helper.Validate;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.HashMap;
import java.util.HashSet;

import java.awt.Image;
import java.awt.image.BufferedImage;

import java.io.File;
import java.io.IOException;
import java.io.*;
import java.net.URL; 
import javax.imageio.ImageIO;



public class MyLoader {

	private static HashSet<String> usedLinks = new HashSet<String>();	
	
	private static HashSet<String> imgSrcs = new HashSet<String>();
	
	private static void makeLinksSet(String url, int maxLevel) {
		
		int level = 0;	
		HashSet<String> previousLevelLinks = new HashSet<String>();
		HashSet<String> currentLevelLinks =  new HashSet<String>();				
		currentLevelLinks.add(url);
		
		System.out.println("Level " + level + ":");
        for (String s: currentLevelLinks) System.out.println(s);
        
		while (level < maxLevel) {
		
			++ level; 
			previousLevelLinks.clear();
			previousLevelLinks = currentLevelLinks;
			usedLinks.addAll(previousLevelLinks);
			currentLevelLinks = new HashSet<String>();
			
			for (String s: previousLevelLinks) {
				try {
					Document doc = Jsoup.connect(s).get();
        			Elements links = doc.select("a[href]");   
        			for (Element link : links) {
        				String href = link.attr("abs:href");        				
        				if (! usedLinks.contains(href)) currentLevelLinks.add(href);
        			}
        			        			
        		}
        		catch (Exception e) {}         		      
        	}
        	System.out.println(String.format("Level %d: %d links", level,  currentLevelLinks.size()));
        	for (String s: currentLevelLinks) System.out.println(s);
        	
        }
        usedLinks.addAll(currentLevelLinks);     
	}
	

	private static String loadImage(String url, String directoryName) {
        try {                   
            BufferedImage img = ImageIO.read(new URL(url));
            File directory = new File(directoryName);
            directory.mkdir();
            int c = url.lastIndexOf('/');
            String filename = url.substring(c + 1);
            c = filename.lastIndexOf('.');
            String ext = "png";
            if (c!= -1) {
            	ext = filename.substring(c + 1);            	
            }
            File file = new File(directory, filename);
            if (!file.exists()) {
                file.createNewFile();
            }
            ImageIO.write(img, ext,  file);
            return filename;
            //return img;
        } 
        catch (Exception e) { }
        return null;
    }
    
    private static void expandImgSrcsSet(String url) {
    	try {
    		Document doc = Jsoup.connect(url).get();        	
        	Elements images = doc.select("img");
        	for (Element img : images) {
        		String imgsrc = img.attr("abs:src");
        		imgSrcs.add(imgsrc);            
        	}
        }
        catch (Exception e) {}
 	}
    
    private static void makeImgSrcsSet() {
    	for (String url: usedLinks) {
    		expandImgSrcsSet(url);
    	}
    }
    
    public static void main(String[] args) throws IOException {
        
        String url = args[0];
        int linklevels = 0;
        String directoryName = ".";
        for (int i = 0; i < args.length; ++ i)
        {
        	if (args[i].equals("--output-dir") )
        		directoryName = args[i+1];
        	if (args[i].equals("--links") )
        		linklevels = Integer.parseInt( args[i+1]);
        }       
        print("Making a list of urls...");        
		makeLinksSet(url, linklevels);
		print("Making a list of images..."); 
		makeImgSrcsSet();
		print("Downloading images...");
		int i = 0;
		for (String s: imgSrcs) {
        	//System.out.println(s);
        	print("%s ", loadImage(s, directoryName));
        	++i;
        }
        
    }
    private static void print(String msg, Object... args) {
        System.out.println(String.format(msg, args));
    }

    
}
